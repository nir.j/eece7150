#!/usr/bin/env python3
# -*- coding: utf-8 -*-

class Feature_Manager():
    '''
    maintains a list of features and 3D landmark points to estimate pose 
    and 
    '''
    def __init__(self):
        ''' constructor initialisation
        '''
        # initialise a list of keypoints in two previous frames (frame 2 and frame 3)
        #  current frame
        self.kp_frame_c = []
        self.kp_frame_p = []
        self.kp_frame_i = []
        self.des_frame_c = []
        self.des_frame_p = []
        self.des_fram_i = []
    
    def match_features(self,kp1,kp2,des1,des2):
        ''' 
        returns a list of matching source points 
        and destination points
       '''
    
    def calculate_3D_landmarks(self,camera_model):
        ''' 
        Outputs a list of 3d landmarks from the matching features
        '''
    
    def get_keypoint_list(self):
        '''returns a list of keypoints
        '''
    
    def get_landmark_list(self):
        '''
        returns a list of landmarks
        '''
    
    