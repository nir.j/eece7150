# -*- coding: utf-8 -*-

import numpy as np
import cv2 as cv
import matplotlib.pyplot as plt
import os

class Image_Frame:
    is_loaded = False  # false when there is no image stored
    
    def __init__(self,dirpath,filename):
        imgpath = dirpath+'/'+filename
        if os.path.isdir(dirpath) & os.path.exists(imgpath):
            self.__image = cv.imread(imgpath)
            self._is_loaded = True
            self.__sift = cv.xfeatures2d.SIFT_create()
            self.__img_size = self.__image.shape #
            self._kp = [] # list of keypoints in the image
            self._des = [] # list of descriptors for the keypoints in the image
        else:
            self.is_loaded = False
            raise Exception('path does not exist')
            
    def get_image(self):
        '''
        get the raw image stored in the object
        '''
        return self.__image
    
    def set_image(self,dirpath,filename):
        '''
        set the image from the path mentioned
        '''
        imagepath = dirpath+'/'+filename
        if os.path.exists(imagepath):    
            img = cv.imread(imagepath)
            self.__image = img
        else:
            raise Exception('path does not exist')
            
    def get_image_size(self):
        ''' 
        get the size of image
        '''
        return self.__img_size

    def compute_interest_points(self):
        '''
        returns a list of (x,y) coordinates of the interest-points of 
        the image 
        '''
        self.__kp = self.__sift.detect(self.__image)
    
    def compute_kp_descriptors(self):
        '''
        returns a list of the descriptors of all the keypoints
        '''
        if (len(self._kp) > 0):
            self.__kp,self.__des = self.__sift.compute(self.__image,self.__kp)
        else:
            raise Exception('Keypoints not computed')
            
    def get_keypoints_and_descriptors(self):
        return self.__kp,self.__des
            
    def get_rgb_image(self):
        b,g,r = cv.split(self._image)
        img = cv.merge([r,g,b])
        return img
    
    def show_key_points(ax,pts_src,pts_dst,frmt):
        ax[0].plot(pts_dst[:,0],pts_dst[:,1],frmt)
        ax[1].plot(pts_src[:,0],pts_src[:,1],frmt)
        
    def tiled_features():
        '''
        
        '''
    
    def perform_non_max_suppression(self):
        '''
        
        '''    