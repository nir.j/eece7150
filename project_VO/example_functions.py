#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Apr  3 12:04:12 2019

@author: jagat
"""

import cv2 as cv
import numpy as np
import matplotlib.pyplot as plt
folder = 'images'
path1 = folder+'/'+'000000.png'
path2 = folder+'/'+'000002.png'


image_1 = cv.imread(path1)
image_2 = cv.imread(path2)
def match_images(src_img,dst_img):
    '''
    returns the 
    M - homography with respect to the source image
    src_points - matching points in the source image
    dst_points - matching points in the destination image
    '''
    img1 = src_img
    img2 = dst_img
    MIN_MATCH_COUNT = 5

    sift = cv.xfeatures2d.SIFT_create()
    
    kp1, des1 = sift.detectAndCompute(img1,None)
    kp2, des2 = sift.detectAndCompute(img2,None)
    
    FLANN_INDEX_KDTREE = 1
    index_params = dict(algorithm = FLANN_INDEX_KDTREE, trees = 5)
    search_params = dict(checks = 50)
    flann = cv.FlannBasedMatcher(index_params, search_params)
    matches = flann.knnMatch(des1,des2,k=2)
    print(len(matches))
    
    # store all the good matches as per Lowe's ratio test.
    good = []
    for m,n in matches:
        if m.distance < .9*n.distance:
            good.append(m)
    #print(len(good))
    #print(good)
    if len(good)>MIN_MATCH_COUNT:
        src_pts = np.float32([ kp1[m.queryIdx].pt for m in good ]).reshape(-1,1,2)
        dst_pts = np.float32([ kp2[m.trainIdx].pt for m in good ]).reshape(-1,1,2)
        M, mask = cv.findHomography(src_pts, dst_pts, cv.RANSAC,5.0)
        matchesMask = mask.ravel().tolist()
        h,w,d = img1.shape
        pts = np.float32([ [0,0],[0,h-1],[w-1,h-1],[w-1,0] ]).reshape(-1,1,2)
        dst = cv.perspectiveTransform(pts,M)
        img2 = cv.polylines(img2,[np.int32(dst)],True,255,3, cv.LINE_AA)
    else:
        print( "Not enough matches are found - {}/{}".format(len(good), MIN_MATCH_COUNT) )
        matchesMask = None
        
    draw_params = dict(matchColor = (0,255,0), # draw matches in green color
                       singlePointColor = None,
                       matchesMask = matchesMask, # draw only inliers
                       flags = 2)
    img3 = cv.drawMatches(img1,kp1,img2,kp2,good,None,**draw_params)
    #cv.imwrite(name,img3)
    plt.imshow(img3,'gray')
    return M,src_pts,dst_pts

h,src_pts,dst_pts = match_images(image_1,image_2)