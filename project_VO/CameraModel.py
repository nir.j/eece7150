#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Apr 12 20:58:06 2019

@author: jagat
"""
import cv2 as cv 

class Camera_Model():
    def __init__(self,camera_parameter_file):
        ''' constructor initialisation
        '''
    
    def undistort_image(self,image_frame):
        '''loads the object of class image_frame 
        returns the undistorted image_frame object of type image frame class
        '''
    def get_camera_parameters(self):
        ''' returns a dictionary of camera parameters.
        '''
    
    def set_camera_parameters(self,camera_parameter_file):
        '''
        '''            