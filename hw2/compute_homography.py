#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Feb 11 03:22:33 2019

@author: jagat
"""
import numpy as np

def ComputeHomography(pts_src,pts_dst):
    n1 = len(pts_src)
    n2 = len(pts_dst)
    pt= pts_src
    pt_ = pts_dst
    
    # Check if no.of points are same
    if (n1 == n2):
        n = n1
        mat = np.zeros([2*n,8])
        b = np.zeros([2*n,1])
        H = np.zeros([8,1])
        for i in range(n):
            # Conditions for point i
            mat[2*i,:] = [pt[i,0],pt[i,1],1,0,0,0,-pt_[i,0]*pt[i,0],-pt_[i,0]*pt[i,1]]
            mat[2*i+1,:] = [0,0,0,pt[i,0],pt[i,1],1,-pt_[i,1]*pt[i,0],-pt_[i,1]*pt[i,1]]
            b[2*i] = pt_[i][0]
            b[2*i+1] = pt_[i][1]
        
        # Check rank of matrix 
        if (np.linalg.matrix_rank(mat)==8):
            if (n==4):
                H_ = np.matmul(np.linalg.inv(mat),b)
            elif(n>=4):
                H_= np.linalg.lstsq(mat,b,rcond=None)
            else:
                print("Not enought points")
    H = np.append(H_[0],[1])
    H = H.reshape(3,3)
    return H

def ComputeMidpoints(pts):
    midpoint = np.zeros([4,2])
    for i in range(len(pts)):
        if i != 3:
            midpoint[i,:] = (pts[i]+pts[i+1])/2
        else:
            midpoint[i,:] = (pts[i]+pts[0])/2
    return midpoint

def RecursiveMidpoints(pts,n):
    midpoints = np.zeros([4*n,2])
    src_pts = pts
    for i in range(n):  
        midpoints[4*i:4*i+4] = ComputeMidpoints(pts)
        pts = midpoints[4*i:4*i+4]
    pts_= np.concatenate((src_pts,midpoints),axis=0)
    return pts_

def AdjustHomography(img_src,H):
    x_M = img_src.shape[0]
    y_M = img_src.shape[1]
    corners = np.array([[0,x_M,x_M,0],[0,0,y_M,y_M],[1,1,1,1]])
    corners_project = np.matmul(H,corners)
    coord_project_x = np.divide(corners_project[0,:],corners_project[2,:])
    coord_project_y = np.divide(corners_project[1,:],corners_project[2,:])
    delx = int(min(coord_project_x))
    dely = int(min(coord_project_y))
    print(delx)
    print(dely)
    if delx < 0 and  dely < 0:
        H[0,2] = H[0,2] + np.abs(delx)
        H[1,2] = H[1,2] + np.abs(dely)
        return H,np.abs(delx),np.abs(dely)
    elif delx < 0 and dely > 0 :
        H[0,2] = H[0,2] - (delx)
        dely = 0
        return H,np.abs(delx),dely
    elif dely < 0 and delx>0:
        H[1,2] = H[1,2] - (dely)
        delx = 0
        return H,delx,np.abs(dely)
    else:
        delx = 0
        dely = 0
        return H,delx,dely