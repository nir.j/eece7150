#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Feb 19 02:26:11 2019

@author: jagat
"""

import numpy as np
import cv2 as cv
from matplotlib import pyplot as plt
#import open_cv_utilities



def match_images(src_img,dst_img,interest_region):
    img1 = src_img
    img2 = dst_img
    MIN_MATCH_COUNT = 10
    
    # Defined interest region where we may get maximum features. 
    # Some region of images are not good for feature matching
    # such as flat walls we donot want them in our feature matching algorithms
    # To automate this we can use harris corner detector algorithm to search for flat region
    x_min = interest_region[0,0]
    x_max = interest_region[1,0]
    y_min = interest_region[0,1]
    y_max = interest_region[1,1]
    # find the keypoints and descriptors with SIFT
    sift = cv.xfeatures2d.SIFT_create()
    
    kp1, des1 = sift.detectAndCompute(img1[x_min:x_max,y_min:y_max],None)
    kp2, des2 = sift.detectAndCompute(img2[x_min:x_max,y_min:y_max],None)
    
    FLANN_INDEX_KDTREE = 1
    index_params = dict(algorithm = FLANN_INDEX_KDTREE, trees = 5)
    search_params = dict(checks = 50)
    flann = cv.FlannBasedMatcher(index_params, search_params)
    matches = flann.knnMatch(des1,des2,k=2)
    
    # store all the good matches as per Lowe's ratio test.
    good = []
    for m,n in matches:
        if m.distance < 0.7*n.distance:
            good.append(m)
    print(len(good))
    print(good)
    if len(good)>MIN_MATCH_COUNT:
        src_pts = np.float32([ kp1[m.queryIdx].pt for m in good ]).reshape(-1,1,2)
        dst_pts = np.float32([ kp2[m.trainIdx].pt for m in good ]).reshape(-1,1,2)
        M, mask = cv.findHomography(src_pts, dst_pts, cv.RANSAC,5.0)
        matchesMask = mask.ravel().tolist()
        h,w,d = img1.shape
        pts = np.float32([ [0,0],[0,h-1],[w-1,h-1],[w-1,0] ]).reshape(-1,1,2)
        dst = cv.perspectiveTransform(pts,M)
        img2 = cv.polylines(img2,[np.int32(dst)],True,255,3, cv.LINE_AA)
    else:
        print( "Not enough matches are found - {}/{}".format(len(good), MIN_MATCH_COUNT) )
        matchesMask = None
        
    draw_params = dict(matchColor = (0,255,0), # draw matches in green color
                       singlePointColor = None,
                       matchesMask = matchesMask, # draw only inliers
                       flags = 2)
    img3 = cv.drawMatches(img1,kp1,img2,kp2,good,None,**draw_params)
    plt.imshow(img3, 'gray'),plt.show()
    return M

        
filenames = ['room_wall1.jpg',
             'room_wall2.jpg',
             'room_wall3.jpg',
             'room_wall4.jpg'] 
 
#Read images to be stitched
img1 = cv.imread(filenames[0])
img2 = cv.imread(filenames[1])
img3 = cv.imread(filenames[2])
img4 = cv.imread(filenames[3])
#ROI = np.array([img1.shape[0]/2,)
xlim = int(img1.shape[0])
ylim = int(img1.shape[1])

# define region of interest for good number of matches
interest_region_1 = np.array([[0,0],[int(img1.shape[0]),int(img1.shape[1]/1.5)]]) 
interest_region_2 = np.array([[0,0],[int(xlim),int(ylim)]])
interest_region_3 = np.array([[0,0],[xlim,ylim]])

# calculate homographies
H12 = match_images(img1,img2,interest_region_1)
H23 = match_images(img2,img3,interest_region_2)
H34 = match_images(img3,img4,interest_region_3)