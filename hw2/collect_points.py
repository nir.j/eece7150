#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Feb 10 02:43:50 2019

@author: jagat
"""

import cv2 as cv
import numpy as np
from matplotlib import pyplot as plt
pts = np.zeros([4,2])

def OnClick(event):
    print('%s click: button=%d, x=%d, y=%d, xdata=%f, ydata=%f' %
          ('double' if event.dblclick else 'single', event.button,
           event.x, event.y, event.xdata, event.ydata))
    pts.concatenate([event.xdata,event.ydata])
    
    return pt_list

def GetRGBImage(name):
    img = cv.imread(name,cv.IMREAD_COLOR)
    b,g,r = cv.split(img)
    img = cv.merge([r,g,b])
    return img

# Display image 1 and get corner points from frame by clicking the image manually
def ShowPoints(ax,pts_src,pts_dst,frmt):
    ax[0].plot(pts_dst[:,0],pts_dst[:,1],frmt)
    ax[1].plot(pts_src[:,0],pts_src[:,1],frmt)

def ShowImages():
    fig,ax = plt.subplots(1,2)
    ax[0].imshow(img1)  
    ax[1].imshow(img2)
    return ax,fig

def DefineCallbacks(fig):
    cid = fig.canvas.mpl_connect('button_press_event', OnClick)

filenames = ['room_wall1.jpg',
             'room_wall2.jpg',
             'room_wall3.jpg',
             'room_wall4.jpg',
             'image1.jpg',
             'image2.jpg']
img1 = GetRGBImage(filenames[4])
img2 = GetRGBImage(filenames[5])

#Harris corner detection
  
def BGR2RGB(img):
    b,g,r = cv.split(img)
    img = cv.merge([r,g,b])
    return img


