#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Feb 18 19:10:31 2019

@author: jagat
"""
import cv2 as cv
import matplotlib.pyplot as plt
import numpy as np

def get_rgb_image(img):
    b,g,r = cv.split(img)
    img = cv.merge([r,g,b])
    return img

def ShowPoints(ax,pts_src,pts_dst,frmt):
    ax[0].plot(pts_dst[:,0],pts_dst[:,1],frmt)
    ax[1].plot(pts_src[:,0],pts_src[:,1],frmt)

def ShowImages():
    fig,ax = plt.subplots(1,2)
    ax[0].imshow(img1)  
    ax[1].imshow(img2)
    return ax,fig
