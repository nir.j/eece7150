# coding: utf-8

# In[1]:


import numpy as np
import cv2 as cv
import time
import image_stitcher

N = 5
def to_stitch_frame(H_list,img_list,frame_size,translate):
    _warped_img_list = []
    index = 0
    for img in img_list:
        H = np.matmul(translate,H_list[index])
        warped_img = cv.warpPerspective(img_list[index],H,
                               (frame_size[0],frame_size[1]))
        _warped_img_list.append(warped_img)
        index = index + 1
    return _warped_img_list

def stitch_video_frames(cam):
    cv.startWindowThread()
    img_corners = np.array([4,2]) # pixel corners of every image will be stored in this
    fps = cam.get(cv.CAP_PROP_FPS)   #not quite! figure out real fps
    print("Frames per second", fps)  # with timers
    start_time = time.time()
    h_n = [] # variable to save homographhies of N consecutive frames
    
    frame_size_stitched_i = []
    projected_corners_j = np.zeros([4*N,2])
    # homography of first frame to stitched frame. The first frame and stitched frame are assumed parallel
    h_0_stitched = np.array([[1,0,0],[0,1,0],[0,0,1]]) 
    img_list = []
    i = 0
    while True:
        # The first few frames will be relatively same. I am taking first three frames
        if(time.time() - start_time < .1 ):
            ret_val,img_i = cam.read()
            cv.imshow('webcam image',img_i)
            frame_size_stitched_i = [img_i.shape[0],img_i.shape[1]]
            h_i_0 = np.array([[1,0,0],[0,1,0],[0,0,1]]) ;
            stitched_img = img_i
        else:
            # homography from source to destination frame. The i th frame is the destination frame and i+1 th frame is source frame
            # Final stitched image is parallel to zeroth frame
            # homography from current frame (src - i + 1 th frame) to previous frame (i th frame - destination)
            j = i+1 # these are just counters
            ret_val, img_j = cam.read()  # read frame i + 1
            cv.imshow('webcam image',img_j)
            h_j_i = get_homography_from_matches(img_j,img_i) 
            h_j_0 = h_j_i*h_i_0;
            img_corners = image_stitcher.get_image_corners(img_j)
            projected_corners_j = image_stitcher.project_image_corners(h_j_0,img_corners)
            #print(projected_corners_j)
            limits = image_stitcher.get_xy_extreme(projected_corners_j)
           
            #print(limits)
            print(limits)
            print('\n')
            frame_size_stitched_j,translate = get_frame_size(limits,frame_size_stitched_i)
            print(frame_size_stitched_j)
            # Define translation matrix - affine and perspective
            print(translate)
            translate_affine = np.array([[1,0,translate[0]],[0,1,translate[1]]],dtype=np.float32)
            translate_perspective = np.array([[1,0,translate[0]],[0,1,translate[1]],[0,0,1]],dtype=np.float32)
            
            # Transform images
            
            translated_stitched_img = cv.warpAffine(stitched_img,translate_affine,
                                                    (frame_size_stitched_j[0],frame_size_stitched_j[1]))
           # print(h_j_0)
            print(h_j_0)
            if ( h_j_0[0,2] != 0 or h_j_0[1,2] !=0):
                warped_image_j = image_stitcher.to_stitch_frame(h_j_0,img_j,
                                                [frame_size_stitched_j[0],frame_size_stitched_j[1]],translate_perspective) 
            else:
                print(h_j_0[0:1,0:2])
                warped_image_j = cv.warpAffine(img_j,h_j_0[0:2,0:3],
                                                    (frame_size_stitched_j[0],frame_size_stitched_j[1]))
            alpha = .5
            stitched_img= blend_images(translated_stitched_img,warped_image_j,alpha)
            cv.imshow('stitched image',stitched_img)    
            
            # Update for next iteration in the camera frame
            i = i + 1
            h_i_0 = h_j_0
            img_i = img_j
            frame_size_stitched_i = frame_size_stitched_j
            # Storing homography H21 H31 H41 and so on
        if cv.waitKey(1) == 27: 
            break  # esc to quit
    cv.waitKey(1)
    cam.release()
    cv.destroyAllWindows()
    cv.waitKey(1)

def get_frame_size(limits,frame_size_stitched_i):
    min_x = limits[0]
    max_x = limits[1]
    min_y = limits[2]
    max_y = limits[3]
    del_x = 0
    del_y = 0
    translate = np.zeros([2,1]) # initialise and if everything lies within the current stitched frame.
    frame_size_stitched_j = []
    factor = 1.2 # 
    
    # We need to translate the stitched frame when min_x and min_y are less than zero
    # The frames on the left side of 0th frame will give negative coordinates
    if min_x < 0:
        del_x = int(np.abs(min_x))
        translate[0] = factor*del_x; 
        
    # The frames on the bottom side of 0th frame will give negative coordinates. 
    
    if min_y < 0:
        del_y = int(np.abs(min_y))
        translate[1] = factor*del_y
        
    # The frames on the right side of the 0th frame will give max x coordinates
    if max_x > frame_size_stitched_i[0]:
        del_x = int(max_x - frame_size_stitched_i[0])
        
    # The frames at the top of the 0th frame will give max y coordinates
    if max_y > frame_size_stitched_i[1]:
        del_y = int(max_y - frame_size_stitched_i[1])
    
    
    frame_size_stitched_j = [int(frame_size_stitched_i[0] + (factor*del_x)),
                             int(frame_size_stitched_i[1] + (factor*del_y))]
    return frame_size_stitched_j,translate

def get_homography_from_matches(src_img,dst_img):
    img1 = src_img
    img2 = dst_img
    MIN_MATCH_COUNT = 5

    # find the keypoints and descriptors with SIFT
    sift = cv.xfeatures2d.SIFT_create()
    
    kp1, des1 = sift.detectAndCompute(img1,None)
    kp2, des2 = sift.detectAndCompute(img2,None)
    
    FLANN_INDEX_KDTREE = 1
    index_params = dict(algorithm = FLANN_INDEX_KDTREE, trees = 5)
    search_params = dict(checks = 50)
    flann = cv.FlannBasedMatcher(index_params, search_params)
    matches = flann.knnMatch(des1,des2,k=2)
    
    # store all the good matches as per Lowe's ratio test.
    good = []
    for m,n in matches:
        if m.distance < 0.7*n.distance:
            good.append(m)
    print(len(good))
    if len(good)>MIN_MATCH_COUNT:
        src_pts = np.float32([ kp1[m.queryIdx].pt for m in good ]).reshape(-1,1,2)
        dst_pts = np.float32([ kp2[m.trainIdx].pt for m in good ]).reshape(-1,1,2)
        M, mask = cv.findHomography(src_pts, dst_pts, cv.RANSAC,5.0)
    return M
    
def blend_images(src1,src2,alpha):
    # blend images in one frame
    #dst = .1*src_img_list[0]+.1*src_img_list[1]+.7*src_img_list[2]+.1*src_img_list[3]
    beta = 1 - alpha
    dst = cv.addWeighted(src1,beta,src2,alpha,0)
    return dst

def main():
    cam = cv.VideoCapture(0)
    stitch_video_frames(cam)        

if __name__ == '__main__':
    main()


