#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Feb 11 02:46:42 2019

@author: jagat
"""

#Harris corner detection
import open_cv_utilities
import matplotlib.pyplot as plt
import cv2 as cv
import numpy as np

def find_corners(img,blk_size,k_size,k,threshold):
    # takes color image as input and gives corners detected by 

    gray = cv.cvtColor(img,cv.COLOR_BGR2GRAY)
    gray = np.float32(gray)
    dst = cv.cornerHarris(gray,blk_size,k_size,k)
    corners = cv.goodFeaturesToTrack(gray,25,0.01,10)
    #result is dilated for marking the corners, not important
    dst = cv.dilate(dst,None)
    # Threshold for an optimal value, it may vary depending on the image.
    img[dst>threshold*dst.max()]=[0,0,255]
    return dst,img,corners

def read_images(filenames):
    img =[]
    index = 0
    for file in filenames:
        img_ = cv.imread(file)
        img.append(img_)
    return img

def show_image_corners():
    filenames = ['room_wall1.jpg',
                 'room_wall2.jpg',
                 'room_wall3.jpg',
                 'room_wall4.jpg'] 
    img = read_images(filenames)
    fig,ax = plt.subplots(1,2)
    _img_loc,_img_corners,corners = find_corners(img[1],blk_size,k_size,k,threshold)
    #fig,ax = plt.subplots()
    _img_corners = open_cv_utilities.get_rgb_image(_img_corners) 
    ax[0].imshow(_img_loc,cmap='gray',vmin=0,vmax=255)
    ax[1].imshow(_img_corners)
    plt.figure(2)
    plt.imshow(_img_corners)
    plt.plot(corners[:,0,0],corners[:,0,1],'*')
    return corners
