#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Feb 13 23:45:49 2019

@author: jagat
"""

import cv2 as cv
import numpy as np
import matplotlib.pyplot as plt
import math
import open_cv_utilities
import os
import pickle

#def get_rgb_image(img):
#    b,g,r = cv.split(img)
#    img = cv.merge([r,g,b])
#    return img

def blend_images(src_img_list,alpha):
    # blend images in one frame
    index =0
    #dst = .1*src_img_list[0]+.1*src_img_list[1]+.7*src_img_list[2]+.1*src_img_list[3]
    for img in src_img_list:
        if index==0:
            dst = src_img_list[index]
        else:
            beta = 1 - alpha
            dst = cv.addWeighted(dst,beta,src_img_list[index],alpha,20)
        index = index + 1
    return dst
        
# returns a list of warped images in stitched frame
def to_stitch_frame(H_list,img_list,frame_size,translate):
    _warped_img_list = []
    index = 0
    for img in img_list:
        H = np.matmul(translate,H_list[index])
        warped_img = cv.warpPerspective(img_list[index],H,
                               (frame_size[0],frame_size[1]))
        _warped_img_list.append(warped_img)
        index = index + 1
    return _warped_img_list

def load_points(filename):
    pts = np.load(filename)
    pts_left = pts[0:4,:]
    pts_right= pts[4:8,:]
    return pts_left,pts_right

def get_xy_extreme(_all_corners):
    _min_x = min(_all_corners[:,0])
    _max_x = max(_all_corners[:,0])
    _min_y = min(_all_corners[:,1])
    _max_y = max(_all_corners[:,1])
    lim = np.array([_min_x,_max_x,_min_y,_max_y])
    return lim

def project_image_corners(H,img_corners):
    # Find projected corners of the image from the homography matrix h
    projected_corners = np.zeros([4,2])
    corners_ = np.transpose(np.append(img_corners,np.ones([4,1]),axis = 1))
    crnrs_p = np.matmul(H,corners_)
    projected_corners[:,0] = np.divide(crnrs_p[0,:],crnrs_p[2,:]).T
    projected_corners[:,1] = np.divide(crnrs_p[1,:],crnrs_p[2,:]).T
    return projected_corners

def get_image_corners(image):
    #Computes the corners of image in anti-clockwise sense, taking origin as top-right
    img_corners = np.zeros([4,2])
    img_corners[1,:] = np.array([0,image.shape[1]])
    img_corners[2,:] = np.array([image.shape[0],image.shape[1]])
    img_corners[3,:] = np.array([image.shape[0],0])
    return img_corners

def stitch_image(mode):
    
    filenames = ['ESC.970622_025500.0621.tif',
             'ESC.970622_025513.0622.tif',
             'ESC.970622_025526.0623.tif',
             'ESC.970622_030140.0651.tif',
             'ESC.970622_030153.0652.tif',
             'ESC.970622_030206.0653.tif'] 
    
    folder = 'data/data_6/images'
    files = sorted(os.listdir(folder))
    dict_file = open(os.path.join(folder,files),"rb")
    h_dict = pickle.load(dict_file)
    dict_file.close()
    corners = get_image_corners(img1)
    H_i_mid = []
    H_list = list(H_dict.values())
    middle = int(len(file)/2)-1
    for i in range(len(H_dict)):
        if i < mid:
            H_i_mid.append(np.prod(H_list[i:mid-1]))
            prj_corners_i_mid = project_image_corners(H_i_mid[i],corners)
        elif i > mid:
            H_i_mid.append(np.prod(H_list[i:mid-1]))
            prj_corners_i_mid = project_image_corners(H_i_mid[i],corners)
        else:
            H_i_mid = np.identity(3)
    
        
    #Read images to be stitched
    img1 = cv.imread(folder+'/'+filenames[0])
    img2 = cv.imread(folder+'/'+filenames[1])
    img3 = cv.imread(folder+'/'+filenames[2])
    img4 = cv.imread(folder+'/'+filenames[3])
    img5 = cv.imread(folder+'/'+filenames[4])
    img6 = cv.imread(folder+'/'+filenames[5])
    
    # Get image corners of each image
    # Since they are taken from same camera shape of all images is same 
    corners = get_image_corners(img1)
    
    if mode == 0:
    ## for manual selection
        pt_filenames = ['pts_12.npy','pts_23.npy','pts_34.npy']
        # Load matching points of images
        p_12,p_21 = load_points(pt_filenames[0])
        p_23,p_32 = load_points(pt_filenames[1])
        p_34,p_43 = load_points(pt_filenames[2])
        
        # Compute homography matrix
        h12,status = cv.findHomography(p_12,p_21)
        h23,status = cv.findHomography(p_23,p_32)
        h34,status = cv.findHomography(p_34,p_43)
        
    elif mode == 1:
        folder = 'homographies_6'
        # precomputed homographies from feature matcher
        h12 = np.load(folder+'/'+'H12.npy')
        h23 = np.load(folder+'/'+'H23.npy')
        h34 = np.load(folder+'/'+'H34.npy')
        h45 = np.load(folder+'/'+'H45.npy')
        h56 = np.load(folder+'/'+'H56.npy')
        homographies = [h12,h23,h34,h45,h56]
    elif mode == 2:
        h12 = compute_homography([218.758,74.8599,0.016524],[192,192,0])
        h23 = compute_homography([241.812,-65.9387,0.32501],[218.758,74.8599,0.016524])
        h34 = compute_homography([620.948,-26.5367,0.467499],[241.812,-65.9387,0.32501])
        h45 = compute_homography([550.523,90.2622,0.462759],[550.523,90.2622,0.462759])
        h56 = compute_homography([463.212,209.194,0.223257],[550.523,90.2622,0.462759])
        homographies = [h12,h23,h34,h45,h56]
        
    elif mode == 4:
        
        h13 = np.matmul(h23,h12)
        h23 = np.linalg.inv(h23)
        h33 = np.identity(3)
        h43 = np.linalg.inv(h34)
        h53 = np.matmul(np.linalg.inv(h34),np.linalg.inv(h45))
        h63 = np.linalg.multi_dot([np.linalg.inv(h34),np.linalg.inv(h45),np.linalg.inv(h56)])
    # Project corners of each image in centre frame - here image 3 is in center frame
    prj_corners_13 = project_image_corners(h13,corners)
    prj_corners_23 = project_image_corners(h23,corners)
    prj_corners_33 = project_image_corners(h33,corners)
    prj_corners_43 = project_image_corners(h43,corners)
    prj_corners_53 = project_image_corners(h53,corners)
    prj_corners_63 = project_image_corners(h63,corners)
    
    # Append all corners in one np array  
    _all_corners = np.append(prj_corners_13,prj_corners_23,axis=0)
    _all_corners = np.append(_all_corners,prj_corners_33,axis=0)
    _all_corners = np.append(_all_corners,prj_corners_43,axis=0)
    _all_corners = np.append(_all_corners,prj_corners_53,axis=0)
    _all_corners = np.append(_all_corners,prj_corners_63,axis=0)
    print(_all_corners)
    # Get extreme points in the stitched image
    xy_limits = get_xy_extreme(_all_corners)
    x_range = xy_limits[1]-xy_limits[0]
    y_range = xy_limits[3]-xy_limits[2]
    frame_size = [int(x_range*1.2),int(y_range)]
    print(frame_size)
    # Make image list and homography list
    H_list = [h13,h23,h43,h53,h63]
    img_list = [img1,img2,img4,img5,img6]
    #print(H_list)
    # Translation of the images in stitched frame
    x_min = min(xy_limits[0:1])
    y_min = min(xy_limits[2:3])
    if x_min < 0 and y_min < 0:
        translate_projective =np.array([[1,0,np.abs(x_min)],[0,1,np.abs(y_min)],[0,0,1]],dtype=np.float32)
        print(translate_projective)
        translate_affine =np.array([[1,0,np.abs(x_min)],[0,1,np.abs(y_min)]],dtype=np.float32)
        _warped_img_stch_list = to_stitch_frame(H_list,img_list,frame_size,translate_projective)
    
    # Translation of the 3rd frame 
    warp3 = cv.warpAffine(img3,translate_affine,
                          (frame_size[0],frame_size[1]))
    
    src_img_list = [_warped_img_stch_list[0],_warped_img_stch_list[1],warp3,_warped_img_stch_list[2],
                    _warped_img_stch_list[3],_warped_img_stch_list[4]]
    # Blend image
    alpha = .25
    dst = blend_images(src_img_list,alpha)
    dst_rgb = open_cv_utilities.get_rgb_image(dst)
    plt.imshow(dst_rgb)
    return homographies
    
def compute_homography(final_pose,initial_pose):
    Tx = final_pose[0] - initial_pose[0]
    Ty = final_pose[1] - initial_pose[1]
    theta = final_pose[2] - initial_pose[2]
    R = np.array([[math.cos(theta),-math.sin(theta)],[math.sin(theta),math.cos(theta)]])
    H = np.zeros([3,3])
    H[:2,:2] = R
    H[0,2] = Tx
    H[1,2] = Ty
    H[2,2] = 1
    return H
stitch_image(1)