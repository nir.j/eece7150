import matplotlib.pyplot as plt
# import the g2o file (calculated separately from this script)
# g2o_file = open('g2o_output/output08.g2o','r')
g2o_file = open('output.g2o','r')

lines = g2o_file.readlines()
g2o_file.close()
x_vertex = []
y_vertex = []
x_edge = [0]
y_edge = [0]
theta_edge = [0]
for line in lines:
    list = line.split(" ")
    if 'VERTEX_SE2' in line: # these don't match for whatever reason
        x_vertex.append( float(list[2]) )
        y_vertex.append( float(list[3]) )
# for line in lines:
    if 'EDGE_SE2' in line:
        x_edge.append( x_edge[-1] + float( list[3]) )
        y_edge.append( y_edge[-1] + float(list[4]) )
        theta_edge.append( theta_edge[-1] + float(list[5]) )

# plot results (before and after g2o on loop closure)
plt.figure()
#old_points, = plt.plot(np.array(x), np.array(y), 'r*-', label='original points')
#edge_points, = plt.plot(x_edge, y_edge, 'b*-', label='after g2o (from edges)')
vertex_points, = plt.plot(x_vertex, y_vertex, 'g*-', label='after g2o (from vertexes)')
plt.xlabel('pixels'), plt.ylabel('pixels')
plt.gca().invert_yaxis() # using image coordinates
plt.gca().set_aspect('equal', 'datalim')
plt.title('Image centers after g2o')
#plt.legend(handles=[old_points, edge_points, vertex_points])
plt.show()