#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Feb 18 19:10:31 2019

@author: jagat
"""
import cv2 as cv
import matplotlib.pyplot as plt
import numpy as np

def get_rgb_image(img):
    b,g,r = cv.split(img)
    img = cv.merge([r,g,b])
    return img

def ShowPoints(ax,pts_src,pts_dst,frmt):
    ax[0].plot(pts_dst[:,0],pts_dst[:,1],frmt)
    ax[1].plot(pts_src[:,0],pts_src[:,1],frmt)

def ShowImages():
    fig,ax = plt.subplots(1,2)
    ax[0].imshow(img1)  
    ax[1].imshow(img2)
    return ax,fig

    from scipy import spatial

def bounding_box(points, min_x=-np.inf, max_x=np.inf, min_y=-np.inf,
                        max_y=np.inf):
    """ Compute a bounding_box filter on the given points

    Parameters
    ----------                        
    points: (n,2) array
        The array containing all the points's coordinates. Expected format:
            array([
                [x1,y1],
                ...,
                [xn,yn]])

    min_i, max_i: float
        The bounding box limits for each coordinate. If some limits are missing,
        the default values are -infinite for the min_i and infinite for the max_i.

    Returns
    -------
    bb_filter : boolean array
        The boolean mask indicating wherever a point should be keept or not.
        The size of the boolean mask will be the same as the number of given points.

    """

    bound_x = np.logical_and(points[:, 0] > min_x, points[:, 0] < max_x)
    bound_y = np.logical_and(points[:, 1] > min_y, points[:, 1] < max_y)

    bb_filter = np.logical_and(bound_x, bound_y)

    return bb_filter


def tiled_features(kp, img_shape, tiley, tilex):
    '''
    Given a set of keypoints, this divides the image into a grid and returns 
    len(kp)/(tilex*tiley) maximum responses within each tell. If that cell doesn't 
    have enough points it will return all of them.
    '''
    feat_per_cell = int(len(kp)/(tilex*tiley))
    HEIGHT, WIDTH = img_shape
    assert WIDTH%tiley == 0, "Width is not a multiple of tilex"
    assert HEIGHT%tilex == 0, "Height is not a multiple of tiley"
    w_width = int(WIDTH/tiley)
    w_height = int(HEIGHT/tilex)
        
    xx = np.linspace(0,HEIGHT-w_height,tilex,dtype='int')
    yy = np.linspace(0,WIDTH-w_width,tiley,dtype='int')
        
    kps = np.array([])
    pts = np.array([keypoint.pt for keypoint in kp])
    kp = np.array(kp)
    
    for ix in xx:
        for iy in yy:
            inbox_mask = bounding_box(pts, iy, iy+w_height, ix, ix+w_height)
            inbox = kp[inbox_mask]
            inbox_sorted = sorted(inbox, key = lambda x:x.response, reverse = True)
            inbox_sorted_out = inbox_sorted[:feat_per_cell]
            kps = np.append(kps,inbox_sorted_out)
    return kps.tolist()

def radial_non_max(kp_list, dist):
    ''' 
    Given a set of Keypoints this finds the maximum response within radial 
    distance from each other 
    '''
    kp = np.array(kp_list)
    kp_mask = np.ones(len(kp), dtype=bool)
    pts = [k.pt for k in kp]
    tree = spatial.cKDTree(pts)
    #print ("len of kp1:",len(kp))
    for i, k in enumerate(kp):
        if kp_mask[i]:
            pt = tree.data[i]
            idx = tree.query_ball_point(tree.data[i], dist, p=2., eps=0, n_jobs=1)
            resp = [kp[ii].response for ii in idx]
            _, maxi = max([(v,i) for i,v in enumerate(resp)])
            del idx[maxi]
            for kp_i in idx:
                kp_mask[kp_i] = False 
    return kp[kp_mask].tolist()