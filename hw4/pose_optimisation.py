#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Mar 27 17:20:27 2019

@author: jagat
"""
import numpy as np
from scipy.io import loadmat
import cv2 as cv
import os
import pickle
import math
from matplotlib import pyplot as plt
from collections import OrderedDict
import pose_graph_optimisation as pgo

NOISE_MEAN = 0
NOISE_VARIANCE = .1 # for a movement of 1 unit the uncertainty in the position is .1 unit

image_path = 'data/data_29/images'
dir_path_seq = 'data/data_29/sequence'
dir_path_cs = 'data/data_29/crosslinks'
filename_seq = '29_sequence.pickle'
filename_cs = '29_crosslinks.pickle'
axis_length = 30
c_o = 'b'
c_i = 'r'
def main():
    
    # get sequence list of images
    sequence_dict,img_size = read_images(image_path) # ordered dictionary
    
    # sequential homographies stored in an ordered dictionary
    h_seq_dict = store_homographies(dir_path_seq,img_size,filename_seq)
    
    # cross-link homographies stored in an ordered dictionary
    h_cross_dict = store_homographies(dir_path_cs,img_size,filename_cs)
    
    # Optimise pose with sequence, sequential homographies and cross-link homographies as input
    pose_i,pose_o = optimize_pose(sequence_dict,h_seq_dict,h_cross_dict)
    
    # Showing results
    fig = plt.figure()
    ax1 = fig.add_subplot(111)
    ax1.set_aspect('equal')
    
    # plot initial pose
    plot_2d_pose_list(ax1,pose_i,'r')
    
    # plot optimised pose
    plot_2d_pose_list(ax1,pose_o,'b')

def optimize_pose(sequence_dict,h_seq_dict,h_cross_dict):
    
    # Get initial sequential pose
    vertex_pose_list = []
    vertex_pose_list,num = get_2D_pose(h_seq_dict,sequence_dict)
    
    # Get sequential edges
    edge_seq_info = []
    edge_seq_pose_list,seq_pair = get_edge(h_seq_dict,sequence_dict)
    
    # Compute sequential edge covariance
    for edge_pose in edge_seq_pose_list:
        edge_seq_info.append(compute_edge_information(edge_pose))
        
    # cross-link homographies
    edge_cross_info = []
    edge_cross_pose_list,cross_pair = get_edge(h_cross_dict,sequence_dict)
    
    # compute crosslink edge covariance
    for edge_pose in edge_cross_pose_list:
        edge_cross_info.append(compute_edge_information(edge_pose))
    
    create_g2o_file(vertex_pose_list,seq_pair,edge_seq_pose_list,
                    edge_seq_info,cross_pair,edge_cross_pose_list,
                    edge_cross_info)
    
    # Initialise optimiser
    optimiser = pgo.PoseGraphOptimization()
    
    # Add vertices to the optimiser
    i = 0
    for pose in vertex_pose_list:
        if i == 0:
            optimiser.add_vertex(i,pose,True)
        else: 
            optimiser.add_vertex(i,pose,False)
        i = i+1
    
    # Add sequential edges to the optimiser
    i = 0
    for edge in edge_seq_pose_list:
        optimiser.add_edge(seq_pair[i],edge,edge_seq_info[i])
        i = i+1

    # Add cross-link edges to the optimiser
    i = 0
    for edge in edge_cross_pose_list:
        optimiser.add_edge(cross_pair[i],edge,edge_cross_info[i])
        i = i+1
    
    # Optimise and store results in a list
    optimiser.optimize()
    optimised_vertex_pose_list=[]
    for id_ in num:
        new_pose = optimiser.get_pose(id_)
        optimised_vertex_pose_list.append(new_pose)
        
    return vertex_pose_list,optimised_vertex_pose_list

def get_2D_pose(h_dict,seq_dict):
    ''' compute the pose of the robot midpoint and store it in a list
        Store the list of homographies in H_list and save it    
    '''
    num = []
    pose_list = []
    first_prj_point = np.array([512,512,1]) # midpoint of the first image
    H_trans = np.array([[1,0,-512],
                        [0,1,-512],
                        [0,0,1]])
    i = 0
    
    H_00 = H_trans
    
    # define initial pose
    x_first = np.matmul(H_trans,first_prj_point)[0]
    y_first = np.matmul(H_trans,first_prj_point)[1]
    theta_first = 0
    initial_pose = [x_first,y_first,theta_first]
    pose_list.append(initial_pose)
    num.append(i)
    
    H_list = []
    H_list.append(np.identity(3))
    print('---computing poses of the robot---')
    for filename in h_dict: 
        h = h_dict[filename]     
        H_0i = np.matmul(h,H_list[i])
        #print('h_frame:'+str(H_0i))
        H_list.append(H_0i)
        next_pose = compute_pose(H_0i,initial_pose)
        pose_list.append(next_pose)
        num.append(i+1)
        i = i+1
    pose_arr = np.asarray(pose_list)
    plt.plot(-pose_arr[:,0], pose_arr[:,1],'*-')
    return pose_list,num

def get_edge(h_dict,sequence_dict):
    edge_list = []
    pair_list = []
    keys = list(h_dict.keys())
    for k in keys:
        src_ = k[0:4]
        dst_ = k[5:9]
        pair = [sequence_dict[src_],sequence_dict[dst_]]
        pair_list.append(pair)
        H = h_dict[k]
        del_x = H[0,2]
        del_y = H[1,2]
        R_corr = compute_rot_mat(H)
        del_theta = math.atan2(-R_corr[0,1],R_corr[0,0])
        edge_list.append([del_x,del_y,del_theta])
    return edge_list,pair_list

def compute_edge_information(edge_pose):
    trans_x = abs(edge_pose[0])
    trans_y = abs(edge_pose[1])
    theta = abs(edge_pose[2])
    cov = NOISE_VARIANCE*np.array([[trans_x,0,0],[0,trans_y*NOISE_VARIANCE,0],[0,0,theta]])
    info = np.linalg.inv(cov)
    return info

def compute_homographies(match_dir_path,size):
    # computes the homographies from feature points stored in .mat files
    # stored in directory path 
    files = sorted(os.listdir(match_dir_path))
    H_dict = OrderedDict()
    for f in files:
        data = loadmat(match_dir_path+'/'+f)
        src_pts = data['ff']
        dst_pts = data['gg']
        H,status = cv.findHomography(src_pts,dst_pts,cv.RANSAC,5.0)
        img_pair = f[5:15]
        #print(img_pair)
        H_dict[img_pair] = H
    return H_dict

def compute_pose(H_0i,pose):
    """
    returns the 2d pose (x,y,theta(rad)) from the homography matrix
    """
    # computes the 2d pose of the robot from homography
    new_pose = []
    prj_pt = np.array([pose[0],pose[1],1])    
    updated_prj_pt = np.matmul(H_0i,prj_pt)
    R_corr = compute_rot_mat(H_0i)
    #compute new pose from homography h0i
    x = updated_prj_pt[0]/updated_prj_pt[2]
    y = updated_prj_pt[1]/updated_prj_pt[2]
    theta = math.atan2(-R_corr[0,1],R_corr[0,0])
    new_pose.append(x)
    new_pose.append(y)
    new_pose.append(theta)
    return new_pose

def compute_rot_mat(H):
    R_uncorrected = H[:2,:2]
    U,S,V = np.linalg.svd(R_uncorrected)
    R_corr =  U@V
    return R_corr

def read_images(image_path):
    # read all images in the directory and store it in image_dict
    sequence_dict = OrderedDict()
    image_files = os.listdir(image_path)
    i = 0
    for filename in sorted(image_files):
        sequence_dict[filename.split('.')[2]]= i 
        i = i + 1
    first_img = cv.imread(image_path+'/'+image_files[0])
    img_size = np.array([first_img.shape[0],first_img.shape[1]])
    return sequence_dict,img_size

def store_homographies(dir_path,img_size,filename):
    # Store homographies in a dictionary for all sequential matches
    print('-----Computing Homographies-----')
    h_dict = compute_homographies(dir_path,img_size)
    print('------------------\n')
    
    # Save homograhpies in a pickle file
    print('------Saving homographies-----')
    dict_file = open(filename,"wb")
    pickle.dump(h_dict, dict_file)
    dict_file.close()
    return h_dict

def plot_2d_pose_list(ax,pose_list,c):
    for next_pose in pose_list:
        origin = next_pose[0:2]
        origin = [-origin[0],origin[1]]
        R = np.array([[math.cos(next_pose[2]),-math.sin(next_pose[2])],
                      [math.sin(next_pose[2]),math.cos(next_pose[2])]])
        plot_pose2_on_axes(ax,origin,R,axis_length,c)
    pose_arr = np.asarray(pose_list)
    ax.plot(-pose_arr[:,0], pose_arr[:,1],c+'*-')
  
def plot_pose2_on_axes(axes,origin,R,axis_length,c):
    """
    Plot a 2D pose,  on given axis 'axes' with given 'axis_length'
    is a 2x3 or 3x3 matrix of the form [R | X] 
    where R is 2d rotation and X is translation vector.
    """
    # get rotation and translaption (center)
    gRp = R  # rotation from pose to global 
    origin = np.asarray(origin)
    # draw the camera axes
    x_axis =  origin + gRp[:, 0] * axis_length
    line = np.append(origin[np.newaxis], x_axis[np.newaxis], axis=0)
    axes.plot(line[:, 0], line[:, 1], 'r-')

    y_axis = origin + gRp[:, 1] * axis_length
    line = np.append(origin[np.newaxis], y_axis[np.newaxis], axis=0)
    axes.plot(line[:, 0], line[:, 1], c)
    axes.set_aspect('equal')

    
def create_g2o_file(vertex_pose_list,seq_pair,edge_seq_pose_list,edge_seq_info,
                    cross_pair,edge_cross_pose_list,edge_cross_info):
    f = open('pose.g2o', "w+")we
    id_ = 0
    for v in vertex_pose_list:
        f.write("VERTEX_SE2 %d %d %d %f\r\n" % (id_, v[0], v[1], v[2]))
        if (id_ == 0):
            f.write("FIX %d\r\n" % id_)
        id_ = id_ + 1
    count = 0
    for e in edge_seq_pose_list:
        i = seq_pair[count][0]
        j = seq_pair[count][1]
        info = edge_seq_info[count]
        f.write("EDGE_SE2 %d %d %d %d %f %f %f %f %f %f %f\r\n" %(i, j, e[0], e[1], e[2], info[0,0],0,0,info[1,1],0,info[2,2]))
        count = count + 1
    count = 0
    for e in edge_cross_pose_list:
        i = cross_pair[count][0]
        j = cross_pair[count][1]
        info = edge_cross_info[count]
        f.write("EDGE_SE2 %d %d %d %d %f %f %f %f %f %f %f\r\n" %(i, j, e[0], e[1], e[2], info[0,0],0,0,info[1,1],0,info[2,2]))
        count = count + 1
    f.close()

if __name__="__main":
    main() 
    
