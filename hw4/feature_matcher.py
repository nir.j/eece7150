#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Feb 19 02:26:11 2019
edited on Fri Feb 13 2021
@author: jagat
"""

import numpy as np
import cv2 as cv
from matplotlib import pyplot as plt
from collections import OrderedDict
import os
import time
import pickle
import argparse as ap


def match_images(src_img,dst_img,interest_region,pair,output_dir):
    img1 = src_img
    img2 = dst_img
    MIN_MATCH_COUNT = 10
    
    # Defined interest region where we may get maximum features. 
    # Some region of images are not good for feature matching
    # such as flat walls we donot want them in our feature matching algorithms
    # To automate this we can use harris corner detector algorithm to search for flat region
    x_min = int(interest_region[0,0])
    x_max = int(interest_region[1,0])
    y_min = int(interest_region[0,1])
    y_max = int(interest_region[1,1])
    
    
    # find the keypoints and descriptors with SIFT
    sift = cv.xfeatures2d.SIFT_create(nfeatures=2000,
                                      contrastThreshold=0.02)
    
    kp1, des1 = sift.detectAndCompute(img1[x_min:x_max,y_min:y_max],None)
    kp2, des2 = sift.detectAndCompute(img2[x_min:x_max,y_min:y_max],None)
    
    #kp1 = open_cv_utilities.tiled_features(kp1,img1.shape,3,3)
    #kp2 = open_cv_utilities.tiled_features(kp2,img2.shape,3,3)
    FLANN_INDEX_KDTREE = 1
    index_params = dict(algorithm = FLANN_INDEX_KDTREE, trees = 5)
    search_params = dict(checks = 50)
    flann = cv.FlannBasedMatcher(index_params, search_params)
    matches = flann.knnMatch(des1,des2,k=2)
    
    # store all the good matches as per Lowe's ratio test.
    good = []
    for m,n in matches:
        if m.distance < .9*n.distance:
            good.append(m)
    print("Good matches in {} : {} out of all {}".format(pair,len(good),len(matches)))
    
    if len(good)>MIN_MATCH_COUNT:
        src_pts = np.float32([ kp1[m.queryIdx].pt for m in good ]).reshape(-1,1,2)
        dst_pts = np.float32([ kp2[m.trainIdx].pt for m in good ]).reshape(-1,1,2)
        M, mask = cv.findHomography(src_pts, dst_pts, cv.RANSAC,5.0)
        matchesMask = mask.ravel().tolist()
        h,w = img1.shape
        pts = np.float32([ [0,0],[0,h-1],[w-1,h-1],[w-1,0] ]).reshape(-1,1,2)
        dst = cv.perspectiveTransform(pts,M)
        img2 = cv.polylines(img2,[np.int32(dst)],True,255,3, cv.LINE_AA)
    else:
        print( "Not enough matches are found - {}/{}".format(len(good), MIN_MATCH_COUNT) )
        matchesMask = None
        
    draw_params = dict(matchColor = (0,255,0), # draw matches in green color
                       singlePointColor = None,
                       matchesMask = matchesMask, # draw only inliers
                       flags = 2)
    img3 = cv.drawMatches(img1,kp1,img2,kp2,good,None,**draw_params)
    name = pair+'.jpg'
    
    cv.imwrite(os.path.join(output_dir,name),img3)
    print("Homography between {} determinant value : {} \n".format(pair,np.linalg.det(M)))
    #print(M)
    #print(\n)
    if (np.abs(np.linalg.det(M))<.05):
        print("Determinant of homography between {} is low: {}\n".format(pair,np.linalg.det(M)))
    return M
    

def save_file(filename,dict_var):
    dict_file = open(filename,"wb")
    pickle.dump(dict_var, dict_file)
    dict_file.close()
    
def match_and_save(folder,output_dir):
    files = sorted(os.listdir(folder))
    #files.append(files[0]) # to make a loop
    
    H_dict = OrderedDict()

    # use only when cross links after initial sequential solution is obtained. 
    img = cv.imread(folder+'/'+files[0])
    interest_region = np.array([[0,0],[int(img.shape[0]),int(img.shape[1])]])
        
    #Read images in sequence
    for i in range(0,len(files)-1): 
        img_src = cv.imread(os.path.join(folder,files[i]),0)
        img_dst = cv.imread(os.path.join(folder,files[i+1]),0)
        src_num = files[i].split('.')[2]
        dst_num = files[i+1].split('.')[2]
        pair = 'match_'+ src_num + '_' + dst_num # 'match_621_622 - key value
        H_dict[pair] = match_images(img_src,img_dst,interest_region,\
                                    pair,output_dir)
        time.sleep(1)
    
    # save homographies
    filename = os.path.join(output_dir,'match_homographies.pickle')
    save_file(filename,H_dict)

if __name__=="__main__":
    
    folder = 'data/data_6/'
    parser = ap.ArgumentParser("Arguments for feature matcher")
    parser.add_argument('-dir',\
                        type=str,\
                        dest='data_folder',\
                        default=os.path.join(folder,'images'),\
                        help='input directory of images')
        
    parser.add_argument('-o',\
                        type=str,\
                        dest='output_folder',\
                        default=os.path.join(folder,'matches'),\
                        help='output directory of homographies and matches')
    
    
    args = parser.parse_args()
    output_dir = args.output_folder
    if not os.path.exists(output_dir):
        os.mkdir(output_dir)
    folder = args.data_folder
    match_and_save(folder,output_dir)
    
    