from matplotlib import pyplot as plt
import cv2
import numpy as np

import vikrant_functions

def plot_images_and_keypoints(keypoints1, image1, keypoints2, image2):
    plt.figure()
    plt.subplot(121)
    plt.imshow(image1), plt.title("image1 and keypoints")
    plt.plot(keypoints1[:,0,0], keypoints1[:,0,1], 'g.')
#     plt.show()
    
#     plt.figure()
    plt.subplot(122)
    plt.imshow(image2), plt.title("image2 and keypoints")
    plt.plot(keypoints2[:,0,0], keypoints2[:,0,1], 'r.')
    plt.show()

def rematch_kp_and_des(kp, des, kp_tiled):
    # match up kp and des again (but only those selected for in tiled_features)
    kp_tiled_matching = []
    tiled_index = 0
    des_tiled = np.zeros((len(kp_tiled), 128), dtype=np.float32)
    for index in range(len(kp)):
        if ( kp[index].pt in [feature.pt for feature in kp_tiled] ):
            # we shouldn't need this but do for some reason (maybe due to duplicates?)
            if ( tiled_index > des_tiled.shape[0] - 1 ):
                break 
            kp_tiled_matching.append(kp[index])
            des_tiled[tiled_index,:] = des[index,:]
            tiled_index += 1
    return kp_tiled_matching, des_tiled

# match features (as per this tutorial: https://docs.opencv.org/3.4/dc/dc3/tutorial_py_matcher.html)
def match_features(img1, img2, minimum_matches, distance_constant):
    # Initiate SIFT detector
    # started w/nfeatures=2000, nOctaveLayers=4, contrastThreshold=0.01
    sift = cv2.xfeatures2d.SIFT_create(nfeatures=2000, nOctaveLayers=4, contrastThreshold=0.005)
    # find the keypoints and descriptors with SIFT
    kp1, des1 = sift.detectAndCompute(img1,None)
    kp2, des2 = sift.detectAndCompute(img2,None)
    
    # make sure that matching features are evenly distributed across the image
    num_divisions = 4 # originally 3
    kp1_tiled = vikrant_functions.tiled_features(kp1, img1.shape[:2], tilex=num_divisions, tiley=num_divisions)
    kp2_tiled = vikrant_functions.tiled_features(kp2, img1.shape[:2], tilex=num_divisions, tiley=num_divisions)
    kp1_tiled_matching, des1_tiled = rematch_kp_and_des(kp1, des1, kp1_tiled)
    kp2_tiled_matching, des2_tiled = rematch_kp_and_des(kp2, des2, kp2_tiled)
        
    # FLANN parameters
    FLANN_INDEX_KDTREE = 1
    index_params = dict(algorithm = FLANN_INDEX_KDTREE, trees = 5)
    search_params = dict(checks=40)   # or pass empty dictionary
    flann = cv2.FlannBasedMatcher(index_params,search_params)
    matches = flann.knnMatch(des1_tiled,des2_tiled,k=2)
    
    good_matches = []
    for m,n in matches:
        if m.distance < distance_constant*n.distance:
            good_matches.append(m)

    src_pts = np.float32([kp1_tiled_matching[m.queryIdx].pt for m in good_matches
                         ]).reshape(-1, 1, 2)
    dst_pts = np.float32([kp2_tiled_matching[m.trainIdx].pt for m in good_matches
                         ]).reshape(-1, 1, 2)
    
#     print("points used for homography (before RANSAC)")
#     plot_images_and_keypoints(src_pts, img1, dst_pts, img2)
    
    assert ( len(good_matches) > minimum_matches ), "Not enough reliable matching points. (only " + str(len(good_matches))
        
    return (src_pts, dst_pts)
